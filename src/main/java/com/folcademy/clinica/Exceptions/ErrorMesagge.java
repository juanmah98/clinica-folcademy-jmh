package com.folcademy.clinica.Exceptions;

public class ErrorMesagge {
    public ErrorMesagge(String message) {
        this.message = message;
    }

    private String message;
    private String detail;
    private String code;
    private String path;

    public String getMessage() {
        return message;
    }

    public String getDetail() {
        return detail;
    }

    public String getCode() {
        return code;
    }

    public String getPath() {
        return path;
    }

    public ErrorMesagge(String message, String detail, String code, String path) {
        this.message = message;
        this.detail = detail;
        this.code = code;
        this.path = path;
    }
}
