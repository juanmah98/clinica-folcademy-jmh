package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Entities.Persona;

import java.util.List;

public interface IPersonaService {
    List<Persona> findAllPersonas();
}
