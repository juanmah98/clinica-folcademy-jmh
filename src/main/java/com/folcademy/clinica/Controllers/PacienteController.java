package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.*;

import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping ( "/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;
    private final PersonaService personaService;

    public PacienteController(PacienteService pacienteService, PersonaService personaService) {
        this.pacienteService = pacienteService;
        this.personaService = personaService;
    }
/*
    @PreAuthorize("hasAnyAuthority('get_pacientes','get_paciente')")
    @GetMapping("")
    public ResponseEntity<List<PacienteEnteroDto>>listarTodo(){
        return ResponseEntity.ok(pacienteService.listarTodos());
    }*/
    @GetMapping("")
    public ResponseEntity<List<Paciente>>listarTodo(){
    return ResponseEntity.ok(pacienteService.findAllPacientes());}


    //@PreAuthorize("hasAnyAuthority('get_pacientes','get_paciente')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteEnteroDto>>listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pagSize,
            @RequestParam(name = "orderField", defaultValue = "idpersona") String orderField

    ){
        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pagSize,orderField));
    }

    //@PreAuthorize("hasAnyAuthority('get_pacientes','get_paciente')")
    @GetMapping("/{idPaciente}") //Nos devuelve solo 1 medico
    public  ResponseEntity<PacienteDto> listarUno(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    //@PreAuthorize("hasAuthority('post_pacientes')")
    @PostMapping("")
    public ResponseEntity<PacienteEnteroDto> agregar(@RequestBody  PacienteEnteroDto dto){
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }



    //@PreAuthorize("hasAuthority('put_pacientes')")
   @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente")int id,
                                                    @RequestBody PacienteEnteroDto dto){
        return ResponseEntity.ok(pacienteService.editar(id,dto));
    }

   // @PreAuthorize("hasAuthority('del_pacientes')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente")int id)
    {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }

}
