package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {

    private final PersonaService personaService;
    private final PersonaMapper personaMapper;

    public MedicoMapper(PersonaService personaService, PersonaMapper personaMapper) {
        this.personaService = personaService;

        this.personaMapper = personaMapper;
    }


    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity) //cuando usamos ofNull, si no se cumple (en este caso devolver una entidad), pasa derecho al .orElse
                .map(           //Al map entra cuando se cumple, entocnes es no nulla
                        ent -> new MedicoDto( //aca creamos un nuevo medico, con cualquier combre como ent,
                                ent.getId(),    // pero asignamos solo los valores que queremos mostrar
                                    /*ent.getNombre(),
                                    ent.getApellido(),*/
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersona(),
                                personaService.listarUno(ent.getIdpersona())

                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){ //en este caso el dtoToEntity sirve para generar un medico nuevo,
        Medico entity = new Medico();       //al crearse solo con esos datos, nombre y apellido en el constructor
        entity.setId(dto.getId());          //se asignan como "" vacio. No influye en nada
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(dto.getIdpersona());
        return entity;
    }



    public MedicoEnteroDto entityToEnteroDto(Medico entity){


            return Optional
                    .ofNullable(entity) //cuando usamos ofNull, si no se cumple (en este caso devolver una entidad), pasa derecho al .orElse
                    .map(           //Al map entra cuando se cumple, entocnes es no nulla
                            ent -> new MedicoEnteroDto( //aca creamos un nuevo medico, con cualquier combre como ent,
                                    ent.getId(),    // pero asignamos solo los valores que queremos mostrar
                                    /*ent.getNombre(),
                                    ent.getApellido(),*/
                                    ent.getProfesion(),
                                    ent.getConsulta(),

                                    personaService.listarUno(ent.getIdpersona())

                            )
                    )
                    .orElse(new MedicoEnteroDto());


    }

    public Medico enteroDtoToEntity(MedicoEnteroDto dto, Persona persona){ //en este caso el dtoToEntity sirve para generar un medico nuevo,
        Medico entity = new Medico();       //al crearse solo con esos datos, nombre y apellido en el constructor
        entity.setId(dto.getId());          //se asignan como "" vacio. No influye en nada
        /*entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());*/
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(persona.getIdpersona());

        return entity;
    }


}
