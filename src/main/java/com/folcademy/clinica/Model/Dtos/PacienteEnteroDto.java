package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteEnteroDto {
    Integer id;
    /*String dni = "";
    String nombre = "";
    String apellido = "";
    String telefono = "";*/
    PersonaDto persona;


}
